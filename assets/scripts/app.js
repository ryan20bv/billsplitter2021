

const chosenElement = document.getElementById('chosen').querySelector('h1');

const basicHeader = document.getElementById('basic');
const intermediateHeader = document.getElementById('intermediate');
const advanceHeader = document.getElementById('advance');

// basic //
const basicDisplay = document.getElementById('basic--Display');
// basic //
// intermediate // 
const intermediateDisplay = document.getElementById('intermediate--Display')
// intermediate // 
const advanceDisplay = document.getElementById('advance--Display');


let arrayFriends = [];
let advanceArray = [];

const clearHandler = () => {
    const allClearInput = document.querySelectorAll('.clear--value');
    const allClearText = document.querySelectorAll('.clear--text');

    const basicRight = basicDisplay.querySelector('#right--Screen').firstElementChild;
    const interRight = intermediateDisplay.querySelector('#right--Screen').firstElementChild;
    const advanceRight = advanceDisplay.querySelector('#right--Screen').firstElementChild
    for(const input of allClearInput){
        input.value = "";
        input.placeholder = "";
    }
    for(const text of allClearText){
        text.textContent = "";
    }
    

    basicRight.style.display = "none";
    interRight.style.display = "none";
    advanceRight.style.display = "none";

    removeEntryInArray();
}

const removeEntryInArray = () => {
    const entryTableIntermediate = intermediateDisplay.querySelector('tbody').querySelectorAll('tr');
    const entryTableAdvance = advanceDisplay.querySelector('tbody').querySelectorAll('tr');
    
    for(const row of entryTableIntermediate){
        row.remove();
    }

    for(const row of entryTableAdvance){
        row.remove();
    }

    arrayFriends = [];
    advanceArray = [];
}


const addSetToBasic = () =>{
    basicHeader.classList.add("set");
    intermediateHeader.classList.remove('set');
    advanceHeader.classList.remove('set');
    chosenElement.textContent = basicHeader.textContent;
    basicDisplay.style.display = "flex";
    intermediateDisplay.style.display = "none";
    advanceDisplay.style.display = "none";
    clearHandler();
    basicSet();
}

const addSetToIntermediate = () => {
    intermediateHeader.classList.add('set');
    basicHeader.classList.remove('set');
    advanceHeader.classList.remove('set');
    chosenElement.textContent = intermediateHeader.textContent;
    basicDisplay.style.display = "none";
    intermediateDisplay.style.display = "flex";
    advanceDisplay.style.display = "none";
    clearHandler();
    intermediateSet();
}

const addSetToAdvance = () => {
    advanceHeader.classList.add('set');
    intermediateHeader.classList.remove('set');
    basicHeader.classList.remove('set');
    chosenElement.textContent = advanceHeader.textContent;
    basicDisplay.style.display = "none";
    intermediateDisplay.style.display = "none";
    advanceDisplay.style.display = "flex";
    clearHandler();
    advanceSet();
}

    /*  start of basic */
const basicSet = () => {
    const basicLeft = basicDisplay.querySelector('#left--Screen').firstElementChild
    const inputBasic = basicLeft.querySelectorAll('input');
    
    const basicRight = basicDisplay.querySelector('#right--Screen').firstElementChild
    const buttonBasic = basicLeft.querySelector('button');

    const spanMoney = basicLeft.querySelector('#money--label--basic');
    const spanPerson = basicLeft.querySelector('#persons--label--basic');


    const calculateBasicHandler = () => {
        const bill = inputBasic[0].value.trim();
        const persons = inputBasic[1].value.trim();
        if( bill === '' ||
            persons === '' ||
            isNaN(bill) ||
            isNaN(persons) ||
            bill < 1 ||
            persons < 1
        ){
        checkUserInputBasic(bill, persons);
        } else {
            const amount = bill/persons;
            showRightUi(amount, persons);
        }
    }

    const checkUserInputBasic = (bill, persons) => {
            if(bill === '' || isNaN(bill) || bill < 1){
                inputBasic[0].placeholder = " Enter number (Bill)!";
                inputBasic[0].value = "";
            }
            if(persons === '' || isNaN(persons) || persons < 1){
                inputBasic[1].placeholder = "  Enter How Many persons";
                inputBasic[1].value = "";
            }
    }

    const updateSpanHandler = () =>{
        spanMoney.textContent = inputBasic[0].value + " unit/s";
        spanMoney.style.color = "blue";
        spanPerson.textContent = inputBasic[1].value;
        spanPerson.style.color = "blue";
    }

    const showRightUi = (amount, persons) => {
        basicRight.style.display = "block";
        const spanH3 = basicRight.querySelector('h3').firstElementChild;
        const spanP = basicRight.querySelector('p').firstElementChild;
        spanH3.textContent = persons;
        spanP.textContent = amount;
    } 
    
    inputBasic[0].addEventListener('input', updateSpanHandler);
    inputBasic[1].addEventListener('input', updateSpanHandler);
    buttonBasic.addEventListener('click', calculateBasicHandler);
}
/*  end of basic */

/*  start of intermediate */
const intermediateSet = () => {
    const interLeft = intermediateDisplay.querySelector('#left--Screen').firstElementChild;
    const interRight = intermediateDisplay.querySelector('#right--Screen').firstElementChild;
    const buttonElement = interLeft.querySelector('button');
    const userInputElements = interLeft.querySelectorAll('input');

    const tableBodyElement = interRight.querySelector('tbody');
    
    // const share = tableBodyElement.querySelector('.share');

    // let arrayFriends = [];
    let totalBill = "";

    const getUserInput = () => {
        const amount = userInputElements[0].value.trim();
        const friendName = userInputElements[1].value.trim();
        spanValue(amount);
            
        if(arrayFriends !== 0 ){
            shareValueUpdate(amount);
        }
        totalBill = amount;
        userInputElements[1].placeholder = "";
        return ({
            amount: amount,
            friendName: friendName
        });      
        
    }

    const checkInputValue = (num, friend ) => {
        if (num === ""){
            userInputElements[0].value = "";
            userInputElements[0].placeholder = "  Enter number";
        }
        if (num < 0){
            userInputElements[0].value = "";
            userInputElements[0].placeholder = "  Enter number greater than 0";
        }
        if (friend === "") {
            userInputElements[1].value = "";
            userInputElements[1].placeholder = "  Enter Name here!";
        }
    }

    const spanValue = (amount) => {
        const moneySpan = document.getElementById('money--label--inter');
        moneySpan.style.color = "blue";
        moneySpan.textContent = amount + " unit/s"
    }

    const addNameHandler = () => {
        const newName = getUserInput()?.friendName;
        const bill = getUserInput()?.amount;
        if( bill === "" ||
            isNaN(bill) ||
            bill < 0 ||
            newName === ""
        ){
            checkInputValue(bill, newName);
        } else {
        interRight.style.display = "block";
        const newEntry = {
            id: Math.random().toString(),
            name: newName
        }

        arrayFriends.push(newEntry);
        userInputElements[1].value = "";
        renderUi(newEntry.name,newEntry.id);
        shareValueUpdate(bill);
        // deleteBtnHandler();
        }
        
    }

    const computeForShare = (num) =>{
        const share = num /  arrayFriends?.length;
        return share;
    }

    const renderUi = (friend,id) => {
        const newRender = document.createElement('tr');
        newRender.innerHTML = `
                <td>${friend}</td>
                <td class= "share"></td>
                <td>
                    <button style="color: red">X</button>
                </td>
        `;
        tableBodyElement.append(newRender);
        const buttonElement = newRender.querySelector('button');
        buttonElement.addEventListener('click', deleteElement.bind(null, id))
    }

    const shareValueUpdate = (bill) => {
        const rowsOfTable = tableBodyElement.querySelectorAll('.share');
        const totalValue = interRight.querySelector('.total').querySelector('p');
        const individualShare = computeForShare(bill);
        for(const share of rowsOfTable){
            share.textContent = individualShare;
        }
        totalValue.textContent = bill;
    }

    const deleteElement = (id) => {
        const allRow = interRight.querySelector('tbody').querySelectorAll('tr');
        let index = 0
        for(const friend of arrayFriends){
            if(friend.id === id){
                break;
            }
            index++;
        }
        allRow[index].remove();
        arrayFriends.splice(index,1);
        shareValueUpdate(totalBill);
    }

    userInputElements[0].addEventListener('input', getUserInput);
    userInputElements[1].addEventListener('input', getUserInput);
    buttonElement.addEventListener('click', addNameHandler);

}
/* finished */
/*  end of intermediate */


/*  Start of advance */
const advanceSet = () => {
    const advanceUserInputs = advanceDisplay.querySelectorAll('input');
    const addEntryBtn = advanceDisplay.querySelector('#add--Entry');
    const rightInfoScreen = advanceDisplay.querySelector('#right--Screen').querySelector('.advance');
    const infoEntryElement = rightInfoScreen.querySelector('table').querySelector('tbody');
    const allTotalValue = rightInfoScreen.querySelector('.total');

    let globalTotalBill = 0;

    const getUserInputAdvance = () => {
        const totalBill = advanceUserInputs[0];
        const anyName = advanceUserInputs[1];
        const chipped = advanceUserInputs[2];
        if( totalBill.value === "" || 
            isNaN(totalBill.value) ||
            totalBill.value < 0)
        {
            totalBill.value = "";
            totalBill.placeholder = "   Enter total bill here"
        } else{
            const totalBillSpan = advanceDisplay.querySelector('#money--label--advance');
            totalBillSpan.textContent = totalBill.value + " unit/s";
            totalBillSpan.style.color = "blue";
            updateEntry(totalBill.value);
            globalTotalBill = totalBill.value
            
        } 
        return {
            entryOne: totalBill.value.trim(),
            entryTwo: anyName.value.trim(),
            entryThree: chipped.value.trim()
        }
        
    }

    const afterAddingNameHandler = () =>{
        const anyName = advanceUserInputs[1];
        const chipped = advanceUserInputs[2];
        anyName.value = "";
        anyName.placeholder = "";
        chipped.value = "";
        chipped.placeholder = "";
    }

    const checkForInputValue = (friend, amount) =>{
        if(isNaN(amount)){
            advanceUserInputs[2].value = "";
            advanceUserInputs[2].placeholder = "  enter any amount here!";
        }
        if(friend === ""){
            advanceUserInputs[1].value = "";
            advanceUserInputs[1].placeholder = "  enter any name here";
        }
    }

    const addEntryHandler = () => {
        const totalBill = getUserInputAdvance().entryOne;
        const newName = getUserInputAdvance().entryTwo;
        const chipped = +getUserInputAdvance().entryThree;
        if( newName === "" ||
            isNaN(chipped)
        ){
            checkForInputValue(newName,chipped)
        } else {
            addToAdvanceArrayHandler(newName,chipped);
            updateEntry(totalBill);
            afterAddingNameHandler();
            rightInfoScreen.style.display = "block";
        }
    } 

    const addToAdvanceArrayHandler = (friend, chipped) => {
        const newEntry = {
            id: Math.random().toString(),
            friend: friend,
            chipped: chipped?chipped:0,
        }

        advanceArray.push(newEntry);
        renderAdvanceUi(newEntry.id, newEntry.friend, newEntry.chipped);
    }
    const renderAdvanceUi = (id, name, chipped) => {
        const newRow = document.createElement('tr');
        newRow.innerHTML = `
            <td>${name}</td>
            <td class="chipped">${chipped}</td>
            <td class="additional"></td>
            <td>
                <button style="color: red">X</button>
            </td>
        `
        infoEntryElement.append(newRow);
        const deleteAdvanceBtn = newRow.lastElementChild.querySelector('button');
        // const chippedAdvanceElement = newRow.querySelector('.chipped');
        deleteAdvanceBtn.addEventListener('click', deleteRow.bind(null,id));
        // chippedAdvanceElement.addEventListener('dblclick', editHandler.bind(null,id));
    }

    const updateEntry = (bill) => {
        const allChippedEntry = infoEntryElement.querySelectorAll('.chipped');
        const allAdditionalEntry = infoEntryElement.querySelectorAll('.additional');
        let totalChipped = 0;
        let index = 0;
        let totalAdditional = 0;

        let initialAdditional = bill?(bill - totalChipped): "";
        let individualShare = (initialAdditional / advanceArray.length);
        

        for(const eachChipped of allChippedEntry){
            totalChipped += (+eachChipped.textContent);

            allAdditionalEntry[index].textContent = bill ?
                (+individualShare) - (+eachChipped.textContent) : 0;

            totalAdditional += (+allAdditionalEntry[index].textContent);
            index++;
        }
        
        allTotalValue.children[1].textContent = totalChipped;
        allTotalValue.children[2].textContent = bill?totalAdditional : 0;
        allTotalValue.children[3].textContent = bill?bill : 0;
    }

    const findIndex = (id) => {
        let index = 0;
        for(const entry of advanceArray){
            if (entry.id === id){

                break;
            }
            index++;
        }
        return index;
    }
    const deleteRow = (id) => {

        let selectedIndex = findIndex(id);

        infoEntryElement.children[selectedIndex].remove();
        advanceArray.splice(selectedIndex,1);

        updateEntry(globalTotalBill);
    }

    // const editHandler = (id) => {
    //     alert("edit:" + id)
    // }
    advanceUserInputs[0].addEventListener('input', getUserInputAdvance);
    advanceUserInputs[1].addEventListener('input', getUserInputAdvance);
    advanceUserInputs[2].addEventListener('input', getUserInputAdvance);
    addEntryBtn.addEventListener('click', addEntryHandler );

}
/*  End of advance */


/* next task is Advance */





addSetToBasic();
// addSetToIntermediate();
// addSetToAdvance();

basicHeader.addEventListener('click', addSetToBasic);
intermediateHeader.addEventListener('click', addSetToIntermediate);
advanceHeader.addEventListener('click', addSetToAdvance);